# DiCoExpress

DiCoExpress is a workspace developed in R to perform RNAseq data analysis.
The **Data** directory contains the input files, the **Results** directory contains a subdirectory 
per project with all the results of the different steps, the **Template_scripts** directory stores a R 
file for each project allowing a semi-automated data analysis and finally, the **Sources** directory contains 
the seven following R functions: 
1. Load_Data_Files: load the count file and the target table
2. Quality_Control: filtering, normalization, and data quality controls
3. GLM_Contrasts: define the statistical model and automatically write all possible contrasts
4. DiffAnalysis_edgeR: perform differential expression analysis using generalized linear models with edgeR package
5. Venn_Intersection_Union: create union or intersection list from several contrasts
6. Coexpression_coseq: co-expression analysis using Gaussian mixture models
7. Enrichment: characterize list of genes with enrichment analysis

## Installation

After downloading DiCoExpress either as a zip file or using the command "git clone" in Shell.
1. Open R or Rstudio (if installed) with R version >=3.5.0 The working directory must be DiCoExpress/Template_scripts
2. Install all CRAN R packages used in DiCoExpress by running the R installation program : 
``source("../Sources/Install_Packages.R")``. If all packages are successfully installed, 
the logical value “TRUE” will be printed on the R Console pane for each package. 
If the logical value “FALSE” is printed, you will need to check the error messages 
in the Console pane to find a solution.

## Attached files

- The **Reference Manual** file:<br/>
a full description of the seven function of DiCoExpress. This document helps the user to understand 
all the arguments of the functions and to use them for his own dataset.

- The **Analysis Tutorial** file:<br/>
a tutorial on how to use DiCoExpress on a *Brassica napus* dataset (Haddad *et al.*, 2019).
It is recommended to use this tutorial to understand how to adapt the script for your own dataset.


