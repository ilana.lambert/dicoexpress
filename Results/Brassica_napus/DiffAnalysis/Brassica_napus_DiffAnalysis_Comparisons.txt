Number of DEG for each contrast:
                                             Contrast Nb_DEG
1                                   [MatureLeaf-Root]  28261
2                                           [NoSi-Si]    218
3                         [NoSi_MatureLeaf-NoSi_Root]  25734
4                             [Si_MatureLeaf-Si_Root]  25757
5                     [MatureLeaf_NoSi-MatureLeaf_Si]    754
6                                 [Root_NoSi-Root_Si]    173
7 [MatureLeaf_NoSi-MatureLeaf_Si]-[Root_NoSi-Root_Si]    106

                                              Contrast Expression Nb_DEG
1                                    [MatureLeaf-Root]         Up  13784
2                                    [MatureLeaf-Root]       Down  14477
3                                            [NoSi-Si]         Up    119
4                                            [NoSi-Si]       Down     99
5                          [NoSi_MatureLeaf-NoSi_Root]         Up  12515
6                          [NoSi_MatureLeaf-NoSi_Root]       Down  13219
7                              [Si_MatureLeaf-Si_Root]         Up  12804
8                              [Si_MatureLeaf-Si_Root]       Down  12953
9                      [MatureLeaf_NoSi-MatureLeaf_Si]         Up    532
10                     [MatureLeaf_NoSi-MatureLeaf_Si]       Down    222
11                                 [Root_NoSi-Root_Si]         Up    102
12                                 [Root_NoSi-Root_Si]       Down     71
13 [MatureLeaf_NoSi-MatureLeaf_Si]-[Root_NoSi-Root_Si]         Up     56
14 [MatureLeaf_NoSi-MatureLeaf_Si]-[Root_NoSi-Root_Si]       Down     50
