#!/bin/bash

read -p "What project do you want to export?" proj
read -p "git ssh url?" giturl
mypath=`pwd`"/.."

cd /tmp
git clone $giturl $proj
cd $proj

mkdir -p Results
cp -r ${mypath}/Results/${proj} Results
mkdir -p Data
cp ${mypath}/Data/${proj}_*.txt Data
mkdir -p Scripts
cp ${mypath}/Scripts/DiCoExpress_${proj}.R Scripts
rm -rf Sources
cp -r ${mypath}/Sources .

msg="analysis generated on "`date`
git add .

git commit -m "$msg"
git push

cd ..
rm -rf /tmp/$proj


