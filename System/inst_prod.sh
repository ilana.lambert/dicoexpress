 #!/bin/bash

read -p "Where the prod version should be installed?" path

if test -d $path
then
    echo "Prod env exists -> updating"
    rm -rf $path/Sources
    cp -r ../Sources $path
    cp -r ../System $path
else
    
    echo "Destination path does not exist -> creating a new prod env"
    mkdir -p $path
    cp -r ../Data $path
    cp -r ../Results $path
    cp -r ../Sources $path
    cp -r ../Template_scripts ${path}/Scripts
    cp -r ../System $path
fi


